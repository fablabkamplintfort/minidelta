# MiniDelta

A small rotary Delta-Robot  

Firmware: https://github.com/TTN-/IcePick-Delta  
Hardware: Ramps 1.4 with Arduino Mega 2560 and DRV8825 Drivers  
Design: Fusion (in repo)  
stl-files (in repo)  
